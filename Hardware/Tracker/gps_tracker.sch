EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Raspberry Pi HAT"
Date ""
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 5834BC4A
P 10050 6850
F 0 "H1" H 9900 6950 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 10050 6700 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 9950 6850 60  0001 C CNN
F 3 "" H 9950 6850 60  0001 C CNN
	1    10050 6850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5834BCDF
P 11050 6850
F 0 "H2" H 10900 6950 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 11050 6700 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 10950 6850 60  0001 C CNN
F 3 "" H 10950 6850 60  0001 C CNN
	1    11050 6850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5834BD62
P 10050 7400
F 0 "H3" H 9900 7500 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 10050 7250 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 9950 7400 60  0001 C CNN
F 3 "" H 9950 7400 60  0001 C CNN
	1    10050 7400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5834BDED
P 11100 7400
F 0 "H4" H 10950 7500 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 11100 7250 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 11000 7400 60  0001 C CNN
F 3 "" H 11000 7400 60  0001 C CNN
	1    11100 7400
	1    0    0    -1  
$EndComp
$Comp
L raspberrypi_hat:OX40HAT J3
U 1 1 58DFC771
P 2600 2250
F 0 "J3" H 2950 2350 50  0000 C CNN
F 1 "40HAT" H 2300 2350 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 2600 2450 50  0001 C CNN
F 3 "" H 1900 2250 50  0000 C CNN
	1    2600 2250
	1    0    0    -1  
$EndComp
Text Label 3150 7400 2    60   ~ 0
P3V3
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J6
U 1 1 58E13683
P 10250 4750
F 0 "J6" H 10250 4900 50  0000 C CNN
F 1 "CONN_02X02" H 10250 4600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 10250 3550 50  0001 C CNN
F 3 "" H 10250 3550 50  0000 C CNN
	1    10250 4750
	1    0    0    -1  
$EndComp
Text Label 9450 4750 0    60   ~ 0
P3V3
Text Label 9450 4850 0    60   ~ 0
P5V
Wire Wire Line
	9450 4750 9950 4750
Wire Wire Line
	9450 4850 9950 4850
Text Label 11150 4750 2    60   ~ 0
P3V3_HAT
Text Label 11150 4850 2    60   ~ 0
P5V_HAT
Wire Wire Line
	10550 4750 10600 4750
Wire Wire Line
	10550 4850 10600 4850
Text Notes 9650 4400 0    60   ~ 0
FLEXIBLE POWER SELECTION
Text Label 11350 2550 2    60   ~ 0
P5V_HAT
Wire Wire Line
	10600 2550 10750 2550
Text Label 9500 2550 0    60   ~ 0
P5V
Wire Wire Line
	9500 2550 9950 2550
Text Notes 9350 1650 0    118  ~ 24
5V Powered HAT Protection
Text Notes 9100 2200 0    60   ~ 0
This is the recommended 5V rail protection for \na HAT with power going to the Pi.\nSee https://github.com/raspberrypi/hats/blob/master/designguide.md#back-powering-the-pi-via-the-j8-gpio-header
$Comp
L raspberrypi_hat:DMG2305UX Q1
U 1 1 58E14EB1
P 10350 2550
F 0 "Q1" V 10500 2700 50  0000 R CNN
F 1 "DMG2305UX" V 10500 2500 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10550 2650 50  0001 C CNN
F 3 "" H 10350 2550 50  0000 C CNN
	1    10350 2550
	0    -1   -1   0   
$EndComp
$Comp
L raspberrypi_hat:DMMT5401 Q2
U 1 1 58E1538B
P 10050 3150
F 0 "Q2" H 10250 3225 50  0000 L CNN
F 1 "DMMT5401" H 10250 3150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 10250 3075 50  0000 L CIN
F 3 "" H 10050 3150 50  0000 L CNN
	1    10050 3150
	-1   0    0    1   
$EndComp
$Comp
L raspberrypi_hat:DMMT5401 Q2
U 2 1 58E153D6
P 10650 3150
F 0 "Q2" H 10850 3225 50  0000 L CNN
F 1 "DMMT5401" H 10850 3150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 10850 3075 50  0000 L CIN
F 3 "" H 10650 3150 50  0000 L CNN
	2    10650 3150
	1    0    0    1   
$EndComp
$Comp
L Device:R R23
U 1 1 58E15896
P 9950 3750
F 0 "R23" V 10030 3750 50  0000 C CNN
F 1 "10K" V 9950 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 9880 3750 50  0001 C CNN
F 3 "" H 9950 3750 50  0001 C CNN
	1    9950 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R24
U 1 1 58E158A1
P 10750 3750
F 0 "R24" V 10830 3750 50  0000 C CNN
F 1 "47K" V 10750 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 10680 3750 50  0001 C CNN
F 3 "" H 10750 3750 50  0001 C CNN
	1    10750 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 58E15A41
P 9950 3950
F 0 "#PWR01" H 9950 3700 50  0001 C CNN
F 1 "GND" H 9950 3800 50  0000 C CNN
F 2 "" H 9950 3950 50  0000 C CNN
F 3 "" H 9950 3950 50  0000 C CNN
	1    9950 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 58E15A9E
P 10750 3950
F 0 "#PWR02" H 10750 3700 50  0001 C CNN
F 1 "GND" H 10750 3800 50  0000 C CNN
F 2 "" H 10750 3950 50  0000 C CNN
F 3 "" H 10750 3950 50  0000 C CNN
	1    10750 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 3950 9950 3900
Wire Wire Line
	10750 3950 10750 3900
Wire Wire Line
	10750 3350 10750 3450
Wire Wire Line
	10350 2800 10350 3450
Wire Wire Line
	10350 3450 10750 3450
Connection ~ 10750 3450
Wire Wire Line
	9950 3350 9950 3500
Wire Wire Line
	10250 3150 10250 3500
Wire Wire Line
	9950 3500 10250 3500
Connection ~ 9950 3500
Wire Wire Line
	10450 3500 10450 3150
Connection ~ 10250 3500
Wire Wire Line
	9950 2950 9950 2550
Connection ~ 9950 2550
Wire Wire Line
	10750 2950 10750 2550
Connection ~ 10750 2550
$Comp
L raspberrypi_hat:CAT24C32 U2
U 1 1 58E1713F
P 2100 5850
F 0 "U2" H 2450 6200 50  0000 C CNN
F 1 "CAT24C32" H 1850 6200 50  0000 C CNN
F 2 "Package_SOIC:SOIC-8_3.9x4.9mm_P1.27mm" H 2100 5850 50  0001 C CNN
F 3 "" H 2100 5850 50  0000 C CNN
	1    2100 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 58E17715
P 2350 7400
F 0 "R6" V 2430 7400 50  0000 C CNN
F 1 "3.9K" V 2350 7400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 2280 7400 50  0001 C CNN
F 3 "" H 2350 7400 50  0001 C CNN
	1    2350 7400
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 58E17720
P 2350 7650
F 0 "R8" V 2430 7650 50  0000 C CNN
F 1 "3.9K" V 2350 7650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 2280 7650 50  0001 C CNN
F 3 "" H 2350 7650 50  0001 C CNN
	1    2350 7650
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 7400 2150 7400
Wire Wire Line
	1250 7650 2150 7650
Wire Wire Line
	2150 7500 1250 7500
Wire Wire Line
	2150 7750 1250 7750
Wire Wire Line
	2150 7750 2150 7650
Connection ~ 2150 7650
Wire Wire Line
	2150 7500 2150 7400
Connection ~ 2150 7400
Wire Wire Line
	2500 7400 2700 7400
Wire Wire Line
	2700 7650 2500 7650
Connection ~ 2700 7400
Text Label 1250 7400 0    60   ~ 0
ID_SD_EEPROM_pu
Text Label 1250 7500 0    60   ~ 0
ID_SD_EEPROM
Text Label 1250 7650 0    60   ~ 0
ID_SC_EEPROM_pu
Text Label 1250 7750 0    60   ~ 0
ID_SC_EEPROM
Wire Wire Line
	3450 6050 2600 6050
Wire Wire Line
	2600 5950 3450 5950
Text Label 3450 6050 2    60   ~ 0
ID_SD_EEPROM_pu
Text Label 3450 5950 2    60   ~ 0
ID_SC_EEPROM_pu
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 58E18D32
P 750 6100
F 0 "J9" H 750 6250 50  0000 C CNN
F 1 "CONN_01X02" V 850 6100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 750 6100 50  0001 C CNN
F 3 "" H 750 6100 50  0000 C CNN
	1    750  6100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R29
U 1 1 58E19E51
P 1550 6250
F 0 "R29" V 1630 6250 50  0000 C CNN
F 1 "10K" V 1550 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 1480 6250 50  0001 C CNN
F 3 "" H 1550 6250 50  0001 C CNN
	1    1550 6250
	-1   0    0    1   
$EndComp
Text Label 2400 5350 2    60   ~ 0
P3V3
Wire Wire Line
	2100 5350 2400 5350
Wire Wire Line
	2100 5350 2100 5450
$Comp
L power:GND #PWR03
U 1 1 58E1A612
P 1050 5750
F 0 "#PWR03" H 1050 5500 50  0001 C CNN
F 1 "GND" H 1050 5600 50  0000 C CNN
F 2 "" H 1050 5750 50  0000 C CNN
F 3 "" H 1050 5750 50  0000 C CNN
	1    1050 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5650 1300 5750
Wire Wire Line
	1050 5650 1300 5650
Wire Wire Line
	1600 5750 1300 5750
Connection ~ 1300 5750
Wire Wire Line
	1300 5850 1600 5850
$Comp
L power:GND #PWR04
U 1 1 58E1AF98
P 1050 6150
F 0 "#PWR04" H 1050 5900 50  0001 C CNN
F 1 "GND" H 1050 6000 50  0000 C CNN
F 2 "" H 1050 6150 50  0000 C CNN
F 3 "" H 1050 6150 50  0000 C CNN
	1    1050 6150
	1    0    0    -1  
$EndComp
Text Notes 3250 5350 0    60   ~ 0
EEPROM WRITE ENABLE
$Comp
L Device:R R7
U 1 1 58E22085
P 10300 4450
F 0 "R7" V 10380 4450 50  0000 C CNN
F 1 "DNP" V 10300 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 10230 4450 50  0001 C CNN
F 3 "" H 10300 4450 50  0001 C CNN
	1    10300 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 58E2218F
P 10300 5100
F 0 "R9" V 10380 5100 50  0000 C CNN
F 1 "DNP" V 10300 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 10230 5100 50  0001 C CNN
F 3 "" H 10300 5100 50  0001 C CNN
	1    10300 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	9950 4850 9950 5100
Wire Wire Line
	9950 5100 10150 5100
Connection ~ 9950 4850
Wire Wire Line
	10450 5100 10600 5100
Wire Wire Line
	10600 5100 10600 4850
Connection ~ 10600 4850
Wire Wire Line
	10600 4750 10600 4450
Wire Wire Line
	10600 4450 10450 4450
Connection ~ 10600 4750
Wire Wire Line
	10150 4450 9950 4450
Wire Wire Line
	9950 4450 9950 4750
Connection ~ 9950 4750
$Comp
L Device:R R11
U 1 1 58E22900
P 1300 6100
F 0 "R11" V 1380 6100 50  0000 C CNN
F 1 "DNP" V 1300 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 1230 6100 50  0001 C CNN
F 3 "" H 1300 6100 50  0001 C CNN
	1    1300 6100
	0    1    1    0   
$EndComp
Text Notes 1550 7050 0    118  ~ 24
Pullup Resistors
Text Notes 2000 4800 0    118  ~ 24
HAT EEPROM
Text Notes 9850 6500 0    118  ~ 24
Mounting Holes
Text Notes 1650 2000 0    118  ~ 24
40-Pin HAT Connector
Text Label 800  4150 0    60   ~ 0
GND
Wire Wire Line
	2000 4150 800  4150
Text Label 800  3550 0    60   ~ 0
ID_SD_EEPROM
Wire Wire Line
	2000 3550 800  3550
Text Label 800  3450 0    60   ~ 0
GND
Wire Wire Line
	2000 3450 800  3450
Text Label 800  2650 0    60   ~ 0
GND
Wire Wire Line
	2000 2650 800  2650
Text Label 800  2250 0    60   ~ 0
P3V3_HAT
Wire Wire Line
	2000 2250 800  2250
Wire Wire Line
	3200 2850 4400 2850
Wire Wire Line
	3200 3150 4400 3150
Wire Wire Line
	3200 3550 4400 3550
Wire Wire Line
	3200 3650 4400 3650
Wire Wire Line
	3200 3850 4400 3850
Text Label 4400 2850 2    60   ~ 0
GND
Text Label 4400 3150 2    60   ~ 0
GND
Text Label 4400 3650 2    60   ~ 0
GND
Text Label 4400 3550 2    60   ~ 0
ID_SC_EEPROM
Text Label 4400 3850 2    60   ~ 0
GND
Text Label 4400 2450 2    60   ~ 0
GND
Wire Wire Line
	3200 2450 4400 2450
Text Label 4400 2350 2    60   ~ 0
P5V_HAT
Wire Wire Line
	3200 2350 4400 2350
Text Label 4400 2250 2    60   ~ 0
P5V_HAT
Wire Wire Line
	3200 2250 4400 2250
Wire Wire Line
	2700 7650 2700 7400
Text Notes 11350 5100 0    60   ~ 0
HAT spec indicates to NEVER\npower the 3.3V pins on the Raspberry Pi \nfrom the HAT header. Only connect the 3.3V\npower from the Pi if the HAT does not have\n3.3V on board.\n\nIF you are designing a board that could\neither be powered by the Pi or from the HAT\nthe jumpers here can be used.\n\nIn most cases, either design the HAT \nto provide the 5V to the Pi and use the\nprotection circuit above OR power the\nHAT from the Pi and directly connect\nthe P3V3 and P5V to the P3V3_HAT and P5V_HAT\npins.
Text Notes 1200 5250 0    60   ~ 0
The HAT spec requires this EEPROM with system information\nto be in place in order to be called a HAT. It should be set up as write\nprotected (WP pin held high), so it may be desirable to either put a \njumper as shown to enable writing, or to hook up a spare IO pin to do so.
Text Notes 1100 7250 0    60   ~ 0
These are just pullup resistors for the I2C bus on the EEPROM.\nThe resistor values are per the HAT spec.
Text Notes 850  1250 0    100  ~ 0
This is based on the official Raspberry Pi spec to be able to call an extension board a HAT.\nhttps://github.com/raspberrypi/hats/blob/master/designguide.md
$Comp
L power:GND #PWR05
U 1 1 58E3CC10
P 2100 6350
F 0 "#PWR05" H 2100 6100 50  0001 C CNN
F 1 "GND" H 2100 6200 50  0000 C CNN
F 2 "" H 2100 6350 50  0000 C CNN
F 3 "" H 2100 6350 50  0000 C CNN
	1    2100 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 6250 2100 6350
Text Label 1800 6550 2    60   ~ 0
P3V3
Wire Wire Line
	1800 6550 1550 6550
Wire Wire Line
	1550 6550 1550 6400
Wire Wire Line
	1450 6050 1550 6050
Wire Wire Line
	1550 6000 1550 6050
Wire Wire Line
	1050 5650 1050 5750
Connection ~ 1300 5650
Wire Wire Line
	1450 6050 1450 6100
Connection ~ 1550 6050
Wire Wire Line
	950  6100 1050 6100
Wire Wire Line
	1050 6150 1050 6100
Connection ~ 1050 6100
Wire Wire Line
	950  6000 1550 6000
Wire Wire Line
	10750 3450 10750 3600
Wire Wire Line
	9950 3500 9950 3600
Wire Wire Line
	10250 3500 10450 3500
Wire Wire Line
	9950 2550 10100 2550
Wire Wire Line
	10750 2550 11350 2550
Wire Wire Line
	2150 7650 2200 7650
Wire Wire Line
	2150 7400 2200 7400
Wire Wire Line
	2700 7400 3150 7400
Wire Wire Line
	1300 5750 1300 5850
Wire Wire Line
	9950 4850 10050 4850
Wire Wire Line
	10600 4850 11150 4850
Wire Wire Line
	10600 4750 11150 4750
Wire Wire Line
	9950 4750 10050 4750
Wire Wire Line
	1300 5650 1600 5650
Wire Wire Line
	1550 6050 1600 6050
Wire Wire Line
	1550 6050 1550 6100
Wire Wire Line
	1050 6100 1150 6100
Text Notes 6200 2050 0    98   ~ 20
NEO 7M 
$Comp
L neo-7M:neo-7m U1
U 1 1 61DFA97F
P 6550 2500
F 0 "U1" H 6878 2446 50  0000 L CNN
F 1 "neo-7m" H 6878 2355 50  0000 L CNN
F 2 "" H 6800 2600 50  0001 C CNN
F 3 "" H 6800 2600 50  0001 C CNN
	1    6550 2500
	1    0    0    -1  
$EndComp
Text Notes 5950 2300 0    50   ~ 0
Neo 7M GPS breakout board\n 
Wire Wire Line
	6150 4200 6500 4200
Wire Wire Line
	6500 4100 6150 4100
Wire Wire Line
	6500 4000 6150 4000
$Comp
L Connector_Generic:Conn_01x04 J10
U 1 1 61E03887
P 6700 4000
F 0 "J10" H 6780 3992 50  0000 L CNN
F 1 "Conn_01x04" H 6780 3901 50  0000 L CNN
F 2 "" H 6700 4000 50  0001 C CNN
F 3 "~" H 6700 4000 50  0001 C CNN
	1    6700 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3900 6150 3900
Text Label 6150 3900 0    50   ~ 0
GND
Text Label 6150 4000 0    50   ~ 0
TX
Text Label 6150 4100 0    50   ~ 0
RX
Wire Wire Line
	3200 2650 4400 2650
Text Label 4400 2650 2    50   ~ 0
RX
Text Label 4400 2550 2    50   ~ 0
TX
Wire Wire Line
	3200 2550 4400 2550
Text Notes 5850 3200 0    98   ~ 20
Generic Connector
Text Notes 5850 3600 0    59   ~ 0
Used to connect the NEO 7M Breakout \nBoard and the HAT Connector. \nNames specified accordingly. \n
Text Notes 6300 4750 0    98   ~ 20
Switch
$Comp
L Connector:XLR3_Switched J?
U 1 1 61EF10D2
P 5550 5750
F 0 "J?" H 5550 6115 50  0000 C CNN
F 1 "XLR3_Switched" H 5550 6024 50  0000 C CNN
F 2 "" H 5550 5850 50  0001 C CNN
F 3 " ~" H 5550 5850 50  0001 C CNN
	1    5550 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 5100 5550 5450
Wire Wire Line
	5850 5750 6200 5750
Text Label 5550 5100 3    49   ~ 0
3V3
$Comp
L Diode:1N4148 D1
U 1 1 61F09840
P 6350 5750
F 0 "D1" H 6350 5533 50  0000 C CNN
F 1 "1N4148" H 6350 5624 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6350 5575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6350 5750 50  0001 C CNN
	1    6350 5750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R14
U 1 1 61F0B2E2
P 6850 5750
F 0 "R14" V 6643 5750 50  0000 C CNN
F 1 "10K" V 6734 5750 50  0000 C CNN
F 2 "" V 6780 5750 50  0001 C CNN
F 3 "~" H 6850 5750 50  0001 C CNN
	1    6850 5750
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 61F0BE26
P 7300 5750
F 0 "R15" V 7093 5750 50  0000 C CNN
F 1 "4.7K" V 7184 5750 50  0000 C CNN
F 2 "" V 7230 5750 50  0001 C CNN
F 3 "~" H 7300 5750 50  0001 C CNN
	1    7300 5750
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 61F0EE59
P 7550 6050
F 0 "R16" H 7620 6096 50  0000 L CNN
F 1 "4.7K" H 7620 6005 50  0000 L CNN
F 2 "" V 7480 6050 50  0001 C CNN
F 3 "~" H 7550 6050 50  0001 C CNN
	1    7550 6050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:S8050 Q?
U 1 1 61F11BD4
P 7900 5750
F 0 "Q?" H 8090 5796 50  0000 L CNN
F 1 "S8050" H 8090 5705 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 8100 5675 50  0001 L CIN
F 3 "http://www.unisonic.com.tw/datasheet/S8050.pdf" H 7900 5750 50  0001 L CNN
	1    7900 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 5750 6500 5750
Wire Wire Line
	7000 5750 7150 5750
Wire Wire Line
	7450 5750 7550 5750
Wire Wire Line
	7550 5900 7550 5750
Connection ~ 7550 5750
Wire Wire Line
	7550 5750 7700 5750
Wire Wire Line
	8000 5950 8000 6300
Wire Wire Line
	7550 6200 7550 6300
Wire Wire Line
	7550 6300 8000 6300
Connection ~ 8000 6300
Wire Wire Line
	8000 6300 8000 6500
Wire Wire Line
	8000 5550 8000 5350
Text Label 8000 6500 1    49   ~ 0
GND
Text Label 8000 5350 1    49   ~ 0
VCC
Text Label 6150 4200 0    49   ~ 0
VCC
Text Label 5550 6350 1    49   ~ 0
GND
Wire Wire Line
	5550 6050 5550 6350
Text Notes 5850 4900 0    49   ~ 0
Switch to power on and off our device\n
$EndSCHEMATC
